var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'CALL admins_getinfo_all;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getAllPrisons = function(prisons_id, callback) {
    var query = 'CALL admins_getinfo_all2(?);';
    var queryData = [prisons_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getAllRank= function(rank_id, callback) {
    var query = 'CALL admins_getinfo_all3(?);';
    var queryData = [rank_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getAllPrisonsAndRank= function(prisons_id, rank_id, callback) {
    var query = 'CALL admins_getinfo_all4(?,?);';
    var queryData = [prisons_id, rank_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(admins_id, callback) {
    var query = 'CALL admins_getinfo(?)';
    var queryData = [admins_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO admins (first_name, last_name, rank_id, phone_no, email, salary, prisons_id) VALUES (?,?,?,?,?,?,?)';
    var queryData = [params.first_name, params.last_name, params.rank_id, params.phone_no, params.email, params.salary, params.prisons_id
    ];

    connection.query(query, queryData, function (err, result) {
        var admins_id = result.insertId;
        callback(err, admins_id);
    });
};

exports.delete = function(admins_id, callback) {
    var query = 'DELETE FROM admins WHERE admins_id = ?';
    var queryData = [admins_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE admins SET first_name = ?, last_name = ?, email = ?, phone_no = ?, rank_id = ?, salary = ?, prisons_id WHERE admins_id = ?';
    var queryData = [params.first_name, params.last_name, params.email, params.phone_no, params.rank_id, params.salary, params.prisons_id, params.admins_id];

    connection.query(query, queryData, function(err, result) {
                callback(err, result);
    });
};

/*  Stored procedure used in this example

    DROP PROCEDURE IF EXISTS admins_getinfo_all;

    DELIMITER //
    CREATE PROCEDURE admins_getinfo_all ()
    BEGIN

      SELECT a.*, p.prisons_name, ar.rank_name FROM admins a
      LEFT JOIN prisons p ON p.prisons_id = a.prisons_id
      LEFT JOIN admins_rank ar ON ar.rank_id = a.rank_id
      ORDER BY a.salary DESC;

      SELECT p.* FROM prisons p
      LEFT JOIN admins a ON a.prisons_id = p.prisons_id
      GROUP BY p.prisons_id;

      SELECT ar.* FROM admins_rank ar
      LEFT JOIN admins a ON a.rank_id = ar.rank_id
      GROUP BY ar.rank_id;

      SELECT AVG(a.salary) AS avg FROM admins a
      LEFT JOIN prisons p ON p.prisons_id = a.prisons_id
      LEFT JOIN admins_rank ar ON ar.rank_id = a.rank_id
      ORDER BY a.salary DESC;

    END //
    DELIMITER ;


    SELECT p.*, a.admins_id FROM prisons p
      LEFT JOIN admins a ON a.prisons_id = p.prisons_id
      GROUP BY p.prisons_id;


    CALL admins_getinfo_all();

    DROP PROCEDURE IF EXISTS admins_getinfo_all2;

    DELIMITER //
    CREATE PROCEDURE admins_getinfo_all2 (_prisons_id INT)
    BEGIN

      SELECT a.*, ar.rank_name, p.prisons_name FROM admins a
        LEFT JOIN admins_rank ar ON ar.rank_id = a.rank_id
        JOIN prisons p ON p.prisons_id = a.prisons_id AND p.prisons_id = _prisons_id
        ORDER BY a.salary DESC;

      SELECT AVG(a.salary) AS avg FROM admins a
        LEFT JOIN admins_rank ar ON ar.rank_id = a.rank_id
          JOIN prisons p ON p.prisons_id = a.prisons_id AND p.prisons_id = _prisons_id
          ORDER BY a.salary DESC;

    END //
    DELIMITER ;

    CALL admins_getinfo_all2(2);


    DROP PROCEDURE IF EXISTS admins_getinfo_all3;

    DELIMITER //
    CREATE PROCEDURE admins_getinfo_all3 (_rank_id INT)
    BEGIN

      SELECT a.*, ar.rank_name, p.prisons_name FROM admins a
      LEFT JOIN prisons p ON p.prisons_id = a.prisons_id
      JOIN admins_rank ar ON ar.rank_id = a.rank_id AND ar.rank_id = _rank_id
      ORDER BY a.salary DESC;

      SELECT AVG(a.salary) AS avg FROM admins a
      LEFT JOIN prisons p ON p.prisons_id = a.prisons_id
      JOIN admins_rank ar ON ar.rank_id = a.rank_id AND ar.rank_id = _rank_id
      ORDER BY a.salary DESC;

    END //
    DELIMITER ;


    DROP PROCEDURE IF EXISTS admins_getinfo_all4;

    DELIMITER //
    CREATE PROCEDURE admins_getinfo_all4 (_prisons_id INT, _rank_id INT)
    BEGIN

      SELECT a.*, ar.rank_name, p.prisons_name FROM admins a
      JOIN prisons p ON p.prisons_id = a.prisons_id AND p.prisons_id = _prisons_id
      JOIN admins_rank ar ON ar.rank_id = a.rank_id AND ar.rank_id = _rank_id;

      SELECT AVG(a.salary) AS avg FROM admins a
      JOIN prisons p ON p.prisons_id = a.prisons_id AND p.prisons_id = _prisons_id
      JOIN admins_rank ar ON ar.rank_id = a.rank_id AND ar.rank_id = _rank_id;

    END //
    DELIMITER ;

    CALL admins_getinfo_all4(1,1);

    DROP PROCEDURE IF EXISTS admins_getinfo;

    DELIMITER //
    CREATE PROCEDURE admins_getinfo (_admins_id int)
    BEGIN

      SELECT * FROM admins WHERE admins.admins_id = _admins_id;

      SELECT a.*, p.* FROM admins a
      LEFT JOIN admins_address aa ON aa.admins_id = a.admins_id
      LEFT JOIN prisons p ON p.prisons_id = aa.prisons_id
      WHERE a.admins_id = _admins_id;

      SELECT ar.rank_name, a.admins_id FROM admins_rank ar
      JOIN admins a ON a.rank_id = ar.rank_id AND admins_id = _admins_id;

    END //
    DELIMITER ;

    CALL admins_getinfo(1);

    DROP PROCEDURE IF EXISTS admins_getinfo2;

    DELIMITER //
    CREATE PROCEDURE admins_getinfo2 (_admins_id int)
    BEGIN

      SELECT * FROM admins WHERE admins.admins_id = _admins_id;

      SELECT ar.*, a2.admins_id FROM admins_rank ar
         LEFT JOIN admins a ON a.rank_id = ar.rank_id
         LEFT JOIN admins_address aa ON aa.admins_id = a.admins_id
         LEFT JOIN admins a2 ON a2.admins_id = aa.admins_id AND a2.admins_id = _admins_id
         WHERE aa.prisons_id IN (SELECT p.prisons_id FROM prisons p
            LEFT JOIN admins_address ar ON ar.prisons_id = p.prisons_id
            JOIN admins a ON a.admins_id = ar.admins_id AND a.admins_id = _admins_id);

    END //
    DELIMITER ;

 */

exports.edit = function(admins_id, callback) {
    var query = 'CALL admins_getinfo2(?)';
    var queryData = [admins_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};