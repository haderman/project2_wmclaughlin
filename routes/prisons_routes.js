var express = require('express');
var router = express.Router();
var prisons_dal = require('../model/prisons_dal');
var login_dal = require('../model/login_dal');



// View All prisons
router.get('/all', function(req, res) {
    prisons_dal.getAll(function (err, result) {
        if (err) {
            res.send(err);
        }
        else {
            login_dal.getCurrentUser(function (err, result2) {
                res.render('prisons/prisonsViewAll', {'result': result, 'user': result2});
            });
        }
    });
});

// View the prisons for the given id
router.get('/', function(req, res){
    if(req.query.prisons_id == null) {
        res.send('prisons_id is null');
    }
    else {
        prisons_dal.getById(req.query.prisons_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result2) {
                    res.render('prisons/prisonsViewById', {'prisons': result[0], 'user': result2});
                });
            }
        });
    }
});

// Return the add a new prisons form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    prisons_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            login_dal.getCurrentUser(function (err, result2) {
                res.render('prisons/prisonsAdd', {'prisons': result,  'user': result2});
            });
        }
    });
});

// View the prisons for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.prisons_name == null) {
        res.send('Prison Name must be provided.');
    }
    else if(req.query.street == null) {
        res.send('Street name must be selected');
    }
    else if(req.query.zip_code == null) {
        res.send('Zip code must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        prisons_dal.insert(req.query, function (err, prisons_id) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result2) {
                    prisons_dal.edit(prisons_id, function (err, result) {
                        res.render('prisons/prisonsUpdate', {
                            prisons: result[0][0],
                            user: result2,
                            was_successful: true
                        });
                    });
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.prisons_id == null) {
        res.send('A prisons id is required');
    }
    else {
        login_dal.getCurrentUser(function (err, result2) {
            prisons_dal.edit(req.query.prisons_id, function (err, result) {
                res.render('prisons/prisonsUpdate', {prisons: result[0][0], user: result2});
            });
        });
    }

});

router.get('/update', function(req, res) {
    prisons_dal.update(req.query, function(err, result){
        res.redirect(302, '/prisons/all');
    });
});

// Delete a prisons for the given prisons_id
router.get('/delete', function(req, res){
    if(req.query.prisons_id == null) {
        res.send('prisons_id is null');
    }
    else {
        prisons_dal.delete(req.query.prisons_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/prisons/all');
            }
        });
    }
});

module.exports = router;
