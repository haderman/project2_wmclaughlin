var express = require('express');
var router = express.Router();
var guard_dal = require('../model/guard_dal');
var login_dal = require('../model/login_dal');


// View All guards
router.get('/all', function(req, res) {
    guard_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            login_dal.getCurrentUser(function (err, result2) {
                res.render('guard/guardViewAll', {'result': result, 'user': result2});
            });
        }
    });

});

// View the guard for the given id
router.get('/', function(req, res){
    if(req.query.guard_id == null) {
        res.send('guard_id is null');
    }
    else {
        guard_dal.getById(req.query.guard_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result2) {
                    res.render('guard/guardViewById', {guard: result[0][0], prisons: result[1], rank: result[2], user: result2});
                });
            }
        });
    }
});

// Return the add a new guard form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    guard_dal.getAll2(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            login_dal.getCurrentUser(function (err, result2) {
                res.render('guard/guardAdd', {
                    'guard': result[0][0],
                    'sectors': result[1],
                    'prisons': result[2],
                    'rank': result[3],
                    'user': result2
                });
            });
        }
    });
});

// View the guard for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.first_name == null) {
        res.send('First Name must be provided.');
    }
    else if(req.query.last_name == null) {
        res.send('Last Name must be provided.');
    }
    else if(req.query.sectors_id == null) {
        res.send('Sector must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        guard_dal.insert(req.query, function(err,guard_id) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result2) {
                    guard_dal.edit(guard_id, function (err, result) {
                        res.render('guard/guardUpdate', {
                            guard: result[0][0],
                            sectors: result[1],
                            prisons: result[2],
                            rank: result[3],
                            user: result2,
                            was_successful: true
                        });
                    });
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.guard_id == null) {
        res.send('A guard id is required');
    }
    else {
        login_dal.getCurrentUser(function (err, result2) {
            guard_dal.edit(req.query.guard_id, function (err, result) {
                res.render('guard/guardUpdate', {
                    guard: result[0][0],
                    sectors: result[1],
                    prisons: result[2],
                    rank: result[3],
                    user: result2
                });
            });
        });
    }

});

router.get('/update', function(req, res) {
    guard_dal.update(req.query, function(err, result){
            res.redirect(302, '/guard/all');
    });
});

// Delete a guard for the given company_id
router.get('/delete', function(req, res){
    if(req.query.guard_id == null) {
        res.send('guard_id is null');
    }
    else {
        guard_dal.delete(req.query.guard_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/guard/all');
            }
        });
    }
});

module.exports = router;
