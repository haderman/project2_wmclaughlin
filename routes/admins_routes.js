var express = require('express');
var router = express.Router();
var admins_dal = require('../model/admins_dal');
var admins_address_dal = require('../model/admins_address_dal');
var login_dal = require('../model/login_dal');


// View All admins
router.get('/all', function(req, res) {
    admins_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            login_dal.getCurrentUser(function (err, result2) {
                res.render('admins/adminsViewAll', {
                    admins: result[0],
                    prisons: result[1],
                    rank: result[2],
                    average: result[3],
                    user: result2
                });
            });
        }
    });

});

router.get('/all2', function(req, res) {
    if(req.query.prisons_id != "" && req.query.rank_id != "") {
        admins_dal.getAll(function(err, result) {
            admins_dal.getAllPrisonsAndRank(req.query.prisons_id, req.query.rank_id, function (err, result2) {
                if (err) {
                    res.send(err);
                }
                else {
                    login_dal.getCurrentUser(function (err, result3) {
                        res.render('admins/adminsViewAll', {
                            admins: result2[0],
                            prisons: result[1],
                            rank: result[2],
                            average: result2[1],
                            user: result3
                        });
                    });
                }
            });
        });
    }
    else if (req.query.prisons_id != "") {
        admins_dal.getAll(function(err, result) {
            admins_dal.getAllPrisons(req.query.prisons_id, function (err, result2) {
                if (err) {
                    res.send(err);
                }
                else {
                    login_dal.getCurrentUser(function (err, result3) {
                        res.render('admins/adminsViewAll', {
                            admins: result2[0],
                            prisons: result[1],
                            rank: result[2],
                            average: result2[1],
                            user: result3
                        });
                    });
                }
            });
        });
    }
    else if (req.query.rank_id != "") {
        admins_dal.getAll(function(err, result) {
            admins_dal.getAllRank(req.query.rank_id, function (err, result2) {
                if (err) {
                    res.send(err);
                }
                else {
                    login_dal.getCurrentUser(function (err, result3) {
                        res.render('admins/adminsViewAll', {
                            admins: result2[0],
                            prisons: result[1],
                            rank: result[2],
                            average: result2[1],
                            user: result3
                        });
                    });
                }
            });
        });
    }
    else
        {
        admins_dal.getAll(function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result2) {
                    res.render('admins/adminsViewAll', {
                        admins: result[0],
                        prisons: result[1],
                        rank: result[2],
                        average: result[3],
                        user: result2
                    });
                });
            }
        });
    }
});

// View the admins for the given id
router.get('/', function(req, res){
    if(req.query.admins_id == null) {
        res.send('admins_id is null');
    }
    else {
        admins_dal.getById(req.query.admins_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result2) {
                    res.render('admins/adminsViewById', {admins: result[0][0], prisons: result[1], rank: result[2], user: result2});
                });
            }
        });
    }
});

// Return the add a new admins form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    admins_address_dal.getAllPrisons(function(err,result) {
        admins_address_dal.getAllRank(function(err,result2) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result3) {
                    res.render('admins/adminsAdd', {'prisons': result, 'rank': result2, 'user': result3});
                });
            }
        });
    });
});

// View the admins for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.first_name == null) {
        res.send('First Name must be provided.');
    }
    else if(req.query.last_name == null) {
        res.send('Last Name must be provided.');
    }
    else if(req.query.email == null) {
        res.send('Email address must be provided');
    }
    else if(req.query.phone_no == null) {
        res.send('Phone number must be provided');
    }
    else if(req.query.rank_id == null) {
        res.send('A position must be selected');
    }
    else if(req.query.prisons_id == null) {
        res.send('Aprison must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        admins_dal.insert(req.query, function(err, admins_id) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result2) {
                    admins_dal.edit(admins_id, function (err, result) {
                        res.render('admins/adminsUpdate', {
                            admins: result[0][0],
                            rank: result[1],
                            user: result2,
                            was_successful: true
                        });
                    });
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.admins_id == null) {
        res.send('A admins id is required');
    }
    else {
        login_dal.getCurrentUser(function (err, result2) {
            admins_dal.edit(req.query.admins_id, function (err, result) {
                res.render('admins/adminsUpdate', {admins: result[0][0], rank: result[1], user: result2});
            });
        });
    }

});

router.get('/update', function(req, res) {
    admins_dal.update(req.query, function(err, result){
        res.redirect(302, '/admins/all');
    });
});

// Delete a admins for the given admins_id
router.get('/delete', function(req, res){
    if(req.query.admins_id == null) {
        res.send('admins_id is null');
    }
    else {
        admins_dal.delete(req.query.admins_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/admins/all');
            }
        });
    }
});

module.exports = router;
