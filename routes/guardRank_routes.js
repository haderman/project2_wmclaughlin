var express = require('express');
var router = express.Router();
var guardRank_dal = require('../model/guardRank_dal');
var adminsRank_dal = require('../model/adminsRank_dal');
var login_dal = require('../model/login_dal');


// View All guardRanks
router.get('/all', function(req, res) {
    guardRank_dal.getAll(function(err, result) {
        guardRank_dal.getAll(function (err, result2) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result3) {
                    res.render('guardRank/guardRankViewAll', {'guard': result, 'guard': result2, 'user': result3});
                });
            }
        });
    });
});

// View the guardRank for the given id
router.get('/', function(req, res){
    if(req.query.rank_id == null) {
        res.send('rank_id is null');
    }
    else {
        guardRank_dal.getById(req.query.rank_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result2) {
                    res.render('guardRank/guardRankViewById', {rank: result[0][0], guard: result[1], user: result2});
                });
            }
        });
    }
});

// Return the add a new guardRank form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    guardRank_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            login_dal.getCurrentUser(function (err, result2) {
                res.render('guardRank/guardRankAdd', {'guardRank': result[0][0], 'user': result2});
            });
        }
    });
});

// View the guardRank for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.rank_name == null) {
        res.send('Rank Name must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        guardRank_dal.insert(req.query, function(err,rank_id) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result2) {
                    guardRank_dal.edit(rank_id, function (err, result) {
                        res.render('guardRank/guardRankUpdate', {guardRank: result[0][0], user: result2, was_successful: true});
                    });
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.rank_id == null) {
        res.send('rank id is required');
    }
    else {
        login_dal.getCurrentUser(function (err, result2) {
            guardRank_dal.edit(req.query.rank_id, function (err, result) {
                res.render('guardRank/guardRankUpdate', {guardRank: result[0][0], user: result2});
            });
        });
    }

});

router.get('/update', function(req, res) {
    guardRank_dal.update(req.query, function(err, result){
        res.redirect(302, '/adminsRank/all');
    });
});

// Delete a guardRank for the given company_id
router.get('/delete', function(req, res){
    if(req.query.rank_id == null) {
        res.send('rank_id is null');
    }
    else {
        guardRank_dal.delete(req.query.rank_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/adminsRank/all');
            }
        });
    }
});

module.exports = router;
