var express = require('express');
var router = express.Router();
var prisoner_dal = require('../model/prisoner_dal');
var login_dal = require('../model/login_dal');


// View All prisoners
router.get('/all', function(req, res) {
    prisoner_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            login_dal.getCurrentUser(function (err, result2) {
                res.render('prisoner/prisonerViewAll', {'result': result, 'user': result2});
            });
        }
    });

});

// View the prisoner for the given id
router.get('/', function(req, res){
    if(req.query.prisoner_id == null) {
        res.send('prisoner_id is null');
    }
    else {
        prisoner_dal.getById(req.query.prisoner_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result2) {
                    res.render('prisoner/prisonerViewById', {
                        prisoner: result[0][0],
                        prisons: result[1],
                        guard: result[2],
                        user: result2
                    });
                });
            }
        });
    }
});

// Return the add a new prisoner form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    prisoner_dal.getAll2(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            login_dal.getCurrentUser(function (err, result2) {
                res.render('prisoner/prisonerAdd', {'prisoner': result[0][0], 'sectors': result[1], 'user':result2});
            });
        }
    });
});

// View the prisoner for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.first_name == null) {
        res.send('First Name must be provided.');
    }
    else if(req.query.last_name == null) {
        res.send('Last Name must be provided.');
    }
    else if(req.query.sectors_id == null) {
            res.send('Sector must be selected');
    }
    else if(req.query.release_date == null) {
        res.send('Release Date must be selected');
    }
    else if(req.query.admit_date == null) {
        res.send('Admit Date must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        prisoner_dal.insert(req.query, function(err,prisoner_id) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result2) {
                    prisoner_dal.edit(prisoner_id, function (err, result) {
                        res.render('prisoner/prisonerUpdate', {
                            prisoner: result[0][0],
                            sectors: result[1],
                            user: result2,
                            was_successful: true
                        });
                    });
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.prisoner_id == null) {
        res.send('A prisoner id is required');
    }
    else {
        login_dal.getCurrentUser(function (err, result2) {
            prisoner_dal.edit(req.query.prisoner_id, function (err, result) {
                res.render('prisoner/prisonerUpdate', {prisoner: result[0][0], sectors: result[1], user: result2});
            });
        });
    }

});

router.get('/update', function(req, res) {
    prisoner_dal.update(req.query, function(err, result){
        res.redirect(302, '/prisoner/all');
    });
});

// Delete a prisoner for the given prisoner_id
router.get('/delete', function(req, res){
    if(req.query.prisoner_id == null) {
        res.send('prisoner_id is null');
    }
    else {
        prisoner_dal.delete(req.query.prisoner_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/prisoner/all');
            }
        });
    }
});

module.exports = router;
